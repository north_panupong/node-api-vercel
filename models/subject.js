const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const SubjectSchema = mongoose.Schema({
    codeSubject: {
        type: String,
    },
    nameSubject: {
        type: String,
    },
    Teacher:{
        type: Array,
    },
    idEdu:{
        type: mongoose.Schema.Types.ObjectId,
    },
    
});

const Subject = module.exports = mongoose.model('subject', SubjectSchema);

module.exports.getSubjectById = function (id, callback) {
    Subject.findById(id, callback);
};

module.exports.addSubject = function (newSubject, callback) {
    newSubject.save(callback);
};

