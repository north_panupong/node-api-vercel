const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

mongoose.connect(config.database,{ useNewUrlParser: true ,useUnifiedTopology: true ,useFindAndModify:false});

mongoose.connection.on('connected',()=>{
    console.log('connected :'+config.database);
});

mongoose.connection.on('error',(err)=>{
    console.log('database error :'+err);
});

const app = express();

const users = require('./routes/users');
const exams = require('./routes/exams');
const subjects = require('./routes/subjects');
const educate = require('./routes/educats');
const uploadfile = require('./routes/upload');

//port number
const port = 3000;

//core middleware
app.use(cors());

//set start floder
//app.use(express.static(path.join(__dirname,'public')));

//body parser middleware
app.use(bodyParser.json());

//passport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users',users);
app.use('/exams',exams);
app.use('/subjects',subjects);
app.use('/educates',educate);
app.use('/upload',uploadfile);

//index route
app.get('/',(req,res)=>{
    res.send('server is running');
});

app.get('/about',(req,res)=>{
    res.send('API running');
});

//start server
app.listen(port,()=>{
    console.log('server is running on port'+port);
});