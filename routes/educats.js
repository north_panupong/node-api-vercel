const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const EduCate = require('../models/edu_cat');

router.post('/create-Edu', (req, res, next) => {
    let newEduCate = new EduCate({
        Education_category: req.body.Education_category,
        GradeLevel: req.body.GradeLevel,
    });
    EduCate.newEduCate(newEduCate, (err, educate) => {
        if (err) {
            res.json({ success: false, msg: 'failed to add' })
        } else {
            res.json({ success: true, msg: 'added' })
        }
    });
});

router.get('/getall-Edu', (req, res) => {
    EduCate.find({}, (err, result) => {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error: 'Error'
            }))
        }
    })
});

router.delete('/delete-educate/:id', (req, res) => {
    try {
        EduCate.findByIdAndDelete(req.params.id)
            .exec()
            .then(doc => {
                if (!doc) { return res.status(404).end(); }
                return res.status(204).end();
            }).catch(err => next(err));
    } catch { }
});

router.put('/update-educate/:id', (req, res, next) => {
    EduCate.findByIdAndUpdate(req.params.id,
        {
            $set: {
                Education_category: req.body.Education_category,
                GradeLevel: req.body.GradeLevel
            }
        },
        {
            new: true
        },
        function (err, updated) {
            if (err) {
                res.json({ success: false, msg: 'failed to add' })
            } else {
                res.json({ success: true, msg: 'added' })
            }
        }
    )
}
);

router.get('/getall-byid/:id', (req, res) => {
    EduCate.findOne({ '_id': req.params.id }, (err, result) => {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error: 'Error'
            }))
        }
    })
});

module.exports = router;