const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Subject = require('../models/subject');

router.post('/create-subject', (req, res, next) => {
    let newSubject = new Subject({
        codeSubject: req.body.codeSubject,
        nameSubject: req.body.nameSubject,
        Teacher: req.body.Teacher,
        idEdu: req.body.idEdu,
    });
    Subject.addSubject(newSubject, (err, examlist) => {
        if (err) {
            res.json({ success: false, msg: 'failed to add' })
        } else {
            res.json({ success: true, msg: 'added' })
        }
    });
});

router.put('/update-subject/:id', (req, res, next) => {
    Subject.findByIdAndUpdate(req.params.id,
        {
            $set: {
                codeSubject: req.body.codeSubject,
                nameSubject: req.body.nameSubject,
                Teacher: req.body.Teacher,
            }
        },
        {
            new: true
        },
        function (err, updated) {
            if (err) {
                res.json({ success: false, msg: 'failed to add' })
            } else {
                res.json({ success: true, msg: 'added' })
            }
        }
    )
}
);

router.get('/getall-sub', (req, res) => {
    Subject.find({}, (err, result) => {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error: 'Error'
            }))
        }
    })
});

router.get('/getall-byid/:id', (req, res) => {
    Subject.find({ 'idEdu': req.params.id }, (err, result) => {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error: 'Error'
            }))
        }
    })
});

router.delete('/delete-subject/:id', (req, res) => {
    try {
        Subject.findByIdAndDelete(req.params.id)
            .exec()
            .then(doc => {
                if (!doc) { return res.status(404).end(); }
                return res.status(204).end();
            }).catch(err => next(err));
    } catch { }
});

router.delete('/delete-subjectByEdu/:id', (req, res) => {
    try {
        Subject.deleteMany({ 'idEdu': req.params.id })
            .exec().then(doc => {
                if (!doc) { return res.status(404).end(); }
                return res.status(204).end();
            }).catch(err => next(err));
    } catch { }
});

module.exports = router;