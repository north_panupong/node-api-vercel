const express = require('express');
const router = express.Router();
const GridFsStorage = require('multer-gridfs-storage');
const crypto = require('crypto');
const config = require('../config/database');
const multer = require('multer');
const path = require('path');
require('dotenv').config();



module.exports = router;