const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');

// register
router.post('/register', (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    lastname: req.body.lastname,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    role: req.body.role
  });
  User.addUser(newUser, (err, user) => {
    if (err) {
      res.json({ success: false, msg: 'failed to register' })
    } else {
      res.json({ success: true, msg: 'user registered' })
    }
  });
});

// Authenticate
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if (err) throw err;
    if (!user) {
      return res.json({ success: false, msg: 'User not found' });
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if (err) throw err;
      if (isMatch) {
        const token = jwt.sign(user.toJSON(), config.secret, {
          expiresIn: 604800 // 1 week
        });

        res.json({
          success: true,
          token: 'JWT ' + token,
          user: {
            id: user._id,
            name: user.name,
            lastname: user.lastname,
            username: user.username,
            email: user.email,
            role: user.role
          }
        });
      } else {
        return res.json({ success: false, msg: 'Wrong password' });
      }
    });
  });
});

//get profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  res.json({ user: req.user });
});

// check duplicate username
router.post('/checkusername', (req, res, next) => {
  const username = req.body.username;

  User.getUserByUsername(username, (err, user) => {
    if (err) throw err;
    if (user) {
      return res.json({ success: false, msg: 'duplicate username' });
    } else {
      return res.json({ success: true, msg: 'registered' });
    }
  });
});

//check match password
router.post('/checkpassword', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if (err) throw err;
    if (!user) {
      return res.json({ success: false, msg: 'User not found' });
    }
    User.comparePassword(password, user.password, (err, isMatch) => {
      if (err) throw err;
      if (isMatch) {

        res.json({
          success: true,
          msg: 'is match'
        });
      } else {
        return res.json({ success: false, msg: 'Wrong password' });
      }
    });
  });

});

// valid
router.get('/valid', (req, res, next) => {
  res.send('VALIDATE');
});

//update user
router.put('/update-user/:id', (req, res, next) => {
  User.findByIdAndUpdate(req.params.id,
    {
      $set: {
        name: req.body.name,
        password: req.body.password,
        Teacher: req.body.Teacher,
      }
    },
    {
      new: true
    },
    function (err, updated) {
      if (err) {
        res.json({ success: false, msg: 'failed to add' })
      } else {
        res.json({ success: true, msg: 'added' })
      }
    }
  )
}
);

//get username by id
router.get('/getUserByID/:id', (req, res) => {
  User.find({ '_id': req.params.id }, (err, result) => {
      if (err) throw err;
      if (result) {
          res.json(result)
      } else {
          res.send(JSON.stringify({
              error: 'Error'
          }))
      }
  })
});



module.exports = router;